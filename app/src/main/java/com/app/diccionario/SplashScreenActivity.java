package com.app.diccionario;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class SplashScreenActivity extends ActionBarActivity {
    private static final int SPLASH_DISPLAY_TIME = 4000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }


    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_splash_screen, container, false);
            final Runnable finalizer = new Runnable() {
                public void run() {
                    skipHome();
                }
            };
            final Handler handler = new Handler();
            handler.postDelayed(finalizer, SPLASH_DISPLAY_TIME);

            ImageView imageView = (ImageView) rootView.findViewById(R.id.imageView);
            imageView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    handler.removeCallbacks(finalizer);
                    skipHome();
                }
            });
            return rootView;
        }

        public void skipHome() {
            Intent intent = new Intent();
            intent.setClass(getActivity(), MainActivity.class);

            getActivity().startActivity(intent);
            getActivity().finish();

            // transition from splash to main menu
            getActivity().overridePendingTransition(R.anim.activityfadein,
                    R.anim.splashfadeout);
        }
    }

}
