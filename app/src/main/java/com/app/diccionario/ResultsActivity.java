package com.app.diccionario;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

public class ResultsActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Bundle extras;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setIcon(R.drawable.logo);
        Bundle args = new Bundle();
        args.putString("title", getIntent().getStringExtra("title"));
        args.putString("description", getIntent().getStringExtra("description"));

        if (savedInstanceState == null) {
            PlaceholderFragment placeholderFragment =  new PlaceholderFragment();
            placeholderFragment.setArguments(args);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container,placeholderFragment )
                    .commit();
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuitem)
    {
        if (menuitem.getItemId() ==android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(menuitem);
    }
    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        public String title;
        public String description;
        public String sample;
        public PlaceholderFragment() {

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            this.title = getArguments().getString("title");
            this.description = getArguments().getString("description");
            View rootView = inflater.inflate(R.layout.fragment_results, container, false);
            ImageButton button = (ImageButton) rootView.findViewById(R.id.imageButton);
            TextView textView = (TextView) rootView.findViewById(R.id.textView);
            TextView textView2 = (TextView) rootView.findViewById(R.id.textView2);
            textView.setText(title);
            textView2.setText(description);
            //Typeface ttf=Typeface.createFromAsset(getActivity().getAssets(), "fonts/calibri.ttf");
            //textView2.setTypeface(ttf);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getActivity().finish();
                }
            });
            return rootView;
        }
    }

}
