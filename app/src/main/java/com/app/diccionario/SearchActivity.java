package com.app.diccionario;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.diccionario.adapter.WordsListAdapter;
import com.app.diccionario.models.Category;
import com.app.diccionario.models.Word;
import com.app.diccionario.repository.CategoryRepository;
import com.app.diccionario.repository.WordRepository;
import com.app.diccionario.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

public class SearchActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setIcon(R.drawable.logo);

        Bundle args = new Bundle();
        args.putString("searchString", getIntent().getStringExtra("searchString"));
        args.putInt("category_id", getIntent().getIntExtra("category_id",-1));

        if (savedInstanceState == null) {
            PlaceholderFragment placeholderFragment = new PlaceholderFragment();
            placeholderFragment.setArguments(args);
                    getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, placeholderFragment)
                    .commit();
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuitem)
    {
        if (menuitem.getItemId() ==android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(menuitem);
    }
    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        public String searchString;
        public WordRepository repos;
        public WordsListAdapter adapter;
        private ArrayList words;
        public Integer categoryId;
        public Utils utils;
        public boolean showFromLocalDb;
        public PlaceholderFragment() {

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            this.searchString = getArguments().getString("searchString");
            this.categoryId = getArguments().getInt("category_id", 0);
            final View rootView = inflater.inflate(R.layout.fragment_search, container, false);
            //Making the search over here
            //Check internet
            utils =  new Utils();
             showFromLocalDb = false;
            if(utils.isOnline(getActivity())){
                String urlStr = String.format(
                        "http://grandeslibros.com.pe/vpilares/index.php?__api=2&search=%s&category=%d",
                        this.searchString,
                        this.categoryId
                );

                try {
                    URL url = new URL(urlStr);
                    URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
                    url = uri.toURL();
                    urlStr = url.toString();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }


                repos = new WordRepository(getActivity());

                //Parsing JSON
                JsonObjectRequest jsObjRequest = new JsonObjectRequest
                        (Request.Method.GET, urlStr, "", new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                CategoryRepository catRepos = new CategoryRepository(getActivity());
                                try {
                                    JSONArray records = response.getJSONArray("records");
                                    int total = records.length();

                                    if(total>0){
                                        ArrayList<Word> words = new ArrayList<>();
                                        for(int i = 0;i<total;i++){
                                            JSONObject jsonWord = records.getJSONObject(i);
                                            Word word = new Word();
                                            Category category = new Category();
                                            category.setId(jsonWord.getInt("category"));
                                            category.setName(catRepos.getNameById(jsonWord.getInt("category")));
                                            word.setId(jsonWord.getInt("id"));
                                            word.setWord(jsonWord.getString("word"));
                                            word.setCategory(category);
                                            word.setDescription(jsonWord.getString("description"));
                                            word.setAlternativeWord(jsonWord.getString("alternative_words"));
                                            String status = jsonWord.getString("status");
                                            if(!status.equals("D")){
                                                repos.create(word);
                                                words.add(word);
                                            }else{
                                                repos.delete(word);
                                            }

                                        }

                                        showResults(words,rootView);
                                    }else{
                                        showResults(new ArrayList<Word>(),rootView);
                                    }
                                } catch (JSONException e) {
                                    showResults(words, rootView);
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                words = (ArrayList<Word>)repos.getSome(searchString,categoryId);
                                showResults(words,rootView);
                            }
                        });
                //Adding to queue

                MySingleton.getInstance(getActivity().getApplicationContext()).addToRequestQueue(jsObjRequest);

                //

            }else{
                repos = new WordRepository(getActivity());
                words = (ArrayList<Word>)repos.getSome(searchString,this.categoryId);
                showResults(words,rootView);
            }


            return rootView;
        }

        //
        private void showResults(ArrayList<Word> words,View rootView){
            ListView listView  = (ListView) rootView.findViewById(R.id.listView);

            TextView txtResult = (TextView) rootView.findViewById(R.id.txtResult);
            TextView textView = (TextView) rootView.findViewById(R.id.textView);
            ProgressBar prgLoading = (ProgressBar) rootView.findViewById(R.id.prgLoading);
            prgLoading.setVisibility(View.GONE);
            textView.setVisibility(View.VISIBLE);
            if(words == null || words.size() == 0 ){
                txtResult.setVisibility(View.VISIBLE);
                listView.setVisibility(View.GONE);
                txtResult.setText(getString(R.string.not_results, this.searchString));
                textView.setText(getString(R.string.results,0));
            }else{
                txtResult.setVisibility(View.GONE);
                textView.setText(getString(R.string.results,words.size()));
                listView.setVisibility(View.VISIBLE);
                adapter = new WordsListAdapter(getActivity(),words);
                listView.setAdapter(adapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        TextView textView = (TextView) view.findViewById( R.id.textView);
                        String description=(String)textView.getTag();
                        String title = textView.getText().toString();
                        Intent intent = new Intent(getActivity(),ResultsActivity.class);
                        intent.putExtra("title",title);
                        intent.putExtra("description",description);
                        startActivity(intent);
                    }
                });
            }

        }

        //
    }


}
