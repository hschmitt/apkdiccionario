package com.app.diccionario.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;


@DatabaseTable(tableName = "words")
public class Word implements Serializable {

    @DatabaseField(generatedId = true)
    public int id;

    @DatabaseField
    public String word;

    @DatabaseField
    public String description;

    public String getAlternativeWord() {
        return alternativeword;
    }

    public void setAlternativeWord(String alternativeWord) {
        this.alternativeword = alternativeWord;
    }

    @DatabaseField
    public String alternativeword;

    @DatabaseField(foreign = true, foreignAutoRefresh=true, maxForeignAutoRefreshLevel=3)
    private Category category;

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    private boolean processed=false;

    public boolean isShowThisRow() {
        return showThisRow;
    }

    public void setShowThisRow(boolean showThisRow) {
        this.showThisRow = showThisRow;
    }

    private boolean showThisRow = false;
    public Word() {
        // ORMLite needs a no-arg constructor
    }

    public Word(String word, String alternativeWord,String description, Category category) {
        this.word = word;
        this.description = description;
        this.category = category;
        this.alternativeword = alternativeWord;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

}