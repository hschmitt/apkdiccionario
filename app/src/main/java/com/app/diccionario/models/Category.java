package com.app.diccionario.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;


@DatabaseTable(tableName = "categories")
public class Category implements Serializable {

    @DatabaseField(generatedId = true)
    public int id;

    @DatabaseField
    public String name;


    public Category() {
        // ORMLite needs a no-arg constructor
    }

    public Category(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}