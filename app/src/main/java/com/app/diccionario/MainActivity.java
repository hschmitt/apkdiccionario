package com.app.diccionario;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.app.diccionario.adapter.CategoriesListAdapter;
import com.app.diccionario.models.Category;
import com.app.diccionario.repository.CategoryRepository;
import com.app.diccionario.repository.WordRepository;

import java.util.ArrayList;

public class MainActivity extends ActionBarActivity {
    public WordRepository repos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setIcon(R.drawable.logo);
        //Create the repo for the words
        repos = new WordRepository(this);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        public EditText editText;
        public CategoryRepository repos;
        private ArrayList<Category> categories;
        private CategoriesListAdapter adapter;
        public Integer cat_id =0;
        public ImageButton imageButton;

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            ListView lv = (ListView) rootView.findViewById(R.id.listView);
            //
            ImageView imageView = (ImageView) rootView.findViewById(R.id.imageView);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent  = new Intent(getActivity(),MessageActivity.class);
                    startActivity(intent);
                }
            });

            ListView listView = (ListView) rootView.findViewById(R.id.listView);
            repos = new CategoryRepository(getActivity());
            categories = (ArrayList<Category>) repos.getAll();
            adapter = new CategoriesListAdapter(getActivity(), categories);
            //
            editText = (EditText) rootView.findViewById(R.id.editText);
            editText.setImeOptions(EditorInfo.IME_ACTION_DONE);


            imageButton = (ImageButton) rootView.findViewById(R.id.imageButton);
            //
            lv.setAdapter(adapter);
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    TextView textView = (TextView) view.findViewById(R.id.txtCategory);
                    String name = (String) textView.getText().toString();
                    cat_id = (Integer) textView.getTag();
                    imageButton.setEnabled(true);
                    editText.setEnabled(true);

                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);

                    editText.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
                    editText.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP , 0, 0, 0));


                    editText.requestFocus();
                }
            });


            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String searchString = editText.getText().toString();
                    /*if (cat_id == 0) {
                        new AlertDialog.Builder(getActivity()).setMessage("Por Favor seleccione una categoria")
                                .setNegativeButton("Aceptar", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                        return;
                    }
                    if (searchString.equals("")) {
                        new AlertDialog.Builder(getActivity()).setMessage("Por favor ingrese una cadena de busqueda ").
                                setNegativeButton("Aceptar", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                        return;
                    }*/

                    Intent intent = new Intent(getActivity(), SearchActivity.class);
                    intent.putExtra("searchString", searchString);


                    intent.putExtra("category_id", cat_id);
                    startActivity(intent);

                }
            });
            return rootView;
        }
    }

}
