package com.app.diccionario.repository;

import android.content.Context;

import com.app.diccionario.models.Word;
import com.app.diccionario.utils.DatabaseHelper;
import com.app.diccionario.utils.DatabaseManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by hacksy on 2/17/14.
 */
public class WordRepository {
    private DatabaseHelper db;
    Dao<Word, Integer> wordsDao;

    public WordRepository(Context ctx) {
        try {
            DatabaseManager dbManager = new DatabaseManager();
            db = dbManager.getHelper(ctx);
            wordsDao = db.getWordsDao();
        } catch (SQLException e) {
            // TODO: Exception Handling
            e.printStackTrace();
        }

    }

    public int create(Word word) {
        try {
             wordsDao.createOrUpdate(word);
            return 1;
        } catch (SQLException e) {
            // TODO: Exception Handling
            e.printStackTrace();
        }
        return 0;
    }

    public int update(Word word) {
        try {
            return wordsDao.update(word);
        } catch (SQLException e) {
            // TODO: Exception Handling
            e.printStackTrace();
        }
        return 0;
    }

    public int delete(Word word) {
        try {
            return wordsDao.delete(word);
        } catch (SQLException e) {
            // TODO: Exception Handling
            e.printStackTrace();
        }
        return 0;
    }

    public List getAll() {
        try {
            return wordsDao.queryForAll();
        } catch (SQLException e) {
            // TODO: Exception Handling
            e.printStackTrace();
        }
        return null;
    }

    public List getSome(String search,Integer categoryId) {
        try {
            QueryBuilder<Word, Integer> qb = wordsDao.queryBuilder();
            if(categoryId>0) {
                if (search.trim().equals("")) {
                    qb.where().eq("category_id", categoryId);
                } else {
                    qb.where().eq("category_id", categoryId).and().like("word", "%" + search.trim() + "%").or()
                            .like("alternativeword", "%" + search.trim() + "%");

                }
            }else{
                qb.where().like("word", "%" + search.trim() + "%").or()
                        .like("alternativeword", "%" + search.trim() + "%");
            }
            qb.orderBy("category_id",true).orderBy("word",true);


            PreparedQuery<Word> pq = qb.prepare();
            android.util.Log.w("query", pq.getStatement());
            return wordsDao.query(pq);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
