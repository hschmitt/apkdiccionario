package com.app.diccionario.repository;

import android.content.Context;

import com.app.diccionario.models.Category;
import com.app.diccionario.utils.DatabaseHelper;
import com.app.diccionario.utils.DatabaseManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

/**
 * Created by hacksy on 2/17/14.
 */
public class CategoryRepository {
    private DatabaseHelper db;
    Dao<Category, Integer> categoriesDao;
    HashMap<Integer,String> catHashMap = new  HashMap<Integer,String>() ;
    public CategoryRepository(Context ctx) {
        try {
            DatabaseManager dbManager = new DatabaseManager();
            db = dbManager.getHelper(ctx);
            categoriesDao = db.getCategoriesDao();

        } catch (SQLException e) {
            // TODO: Exception Handling
            e.printStackTrace();
        }

    }

    public int create(Category category) {
        try {
            return categoriesDao.create(category);
        } catch (SQLException e) {
            // TODO: Exception Handling
            e.printStackTrace();
        }
        return 0;
    }

    public int update(Category category) {
        try {
            return categoriesDao.update(category);
        } catch (SQLException e) {
            // TODO: Exception Handling
            e.printStackTrace();
        }
        return 0;
    }
    public String getNameById(int id){

        if(!catHashMap.containsKey(id)){
            Category cat = new Category();
            try {
                 cat = categoriesDao.queryForId(id);
            } catch (SQLException e) {
                e.printStackTrace();
                cat.setName("");
            }
            catHashMap.put(id, cat.getName());
        }
        return catHashMap.get(id);
    }

    public int delete(Category category) {
        try {
            return categoriesDao.delete(category);
        } catch (SQLException e) {
            // TODO: Exception Handling
            e.printStackTrace();
        }
        return 0;
    }

    public List getAll() {
        try {
            return categoriesDao.queryForAll();
        } catch (SQLException e) {
            // TODO: Exception Handling
            e.printStackTrace();
        }
        return null;
    }
}
