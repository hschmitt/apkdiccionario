package com.app.diccionario.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.diccionario.R;
import com.app.diccionario.models.Category;

import java.util.ArrayList;

/**
 * Created by hacksy on 2/18/14.
 */

public class CategoriesListAdapter extends BaseAdapter {


        private Context mContext;
        private ArrayList<Category> mCategories;
        private LayoutInflater inflater;

        public CategoriesListAdapter(Context c, ArrayList<Category> categories) {
            inflater = LayoutInflater.from(c);
            mContext = c;
            mCategories = categories;
        }

        @Override
        public int getCount() {
            return mCategories.size();
        }

        @Override
        public Object getItem(int position) {
            return mCategories.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            TextView text;

            if (v == null) {

                v = inflater.inflate(R.layout.category_item, parent, false);
                v.setTag(R.id.textView, v.findViewById(R.id.txtCategory));
            }

            text = (TextView) v.getTag(R.id.textView);
            //get the current item and set the text
            Category item = (Category) getItem(position);

            text.setText(item.getName());
            text.setTag(item.getId());
            //Update the image
            ImageView image = (ImageView) v.findViewById(R.id.imgCategory);
            int imageResource;
            switch(item.getId()){
                case 1:
                     imageResource =R.drawable.blue;
                    break;
                case 2:
                    imageResource = R.drawable.green;
                    break;
                case 3:
                    imageResource = R.drawable.yellow;
                    break;
                case 4:
                    imageResource = R.drawable.pink;
                    break;
                default:
                    imageResource = R.drawable.yellow ;

            }
            Drawable res = mContext.getResources().getDrawable(imageResource);
            image.setImageDrawable(res);
            return v;
        }
    }
