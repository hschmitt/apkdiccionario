package com.app.diccionario.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.diccionario.R;
import com.app.diccionario.models.Word;

import java.util.ArrayList;


public class WordsListAdapter extends BaseAdapter {


    private Context mContext;
    private ArrayList<Word> mWords;
    private LayoutInflater inflater;
    private String currentCat = "";
    public WordsListAdapter(Context c, ArrayList<Word> words) {
        inflater = LayoutInflater.from(c);
        mContext = c;
        mWords = words;
    }

    @Override
    public int getCount() {
        return mWords.size();
    }

    @Override
    public Object getItem(int position) {
        return mWords.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        TextView text;

        if (v == null) {

            v = inflater.inflate(R.layout.word_list_item_search, parent, false);
            v.setTag(R.id.textView, v.findViewById(R.id.textView));
        }
        text = (TextView) v.getTag(R.id.textView);


        //get the current item and set the text
        Word item = (Word) getItem(position);

        String currentCat = item.getCategory().getName();
        LinearLayout included = (LinearLayout) v.findViewById(R.id.included);
        included.setVisibility(View.GONE);
        if(item.isShowThisRow() || (!item.isProcessed() && !this.currentCat.equals( currentCat)) ){

                //Show the included layout
                included.setVisibility(View.VISIBLE);
                //Change Title and image of included view
                this.currentCat = currentCat;

                //
                TextView innerText = (TextView) included.findViewById(R.id.txtCategory);
                innerText.setText(currentCat);
                ImageView innerImage = (ImageView) included.findViewById(R.id.imgCategory);
                //

                int imageResource;
                switch(item.getCategory().getId()){
                    case 1:
                        imageResource =R.drawable.blue;
                        break;
                    case 2:
                        imageResource = R.drawable.green;
                        break;
                    case 3:
                        imageResource = R.drawable.yellow;
                        break;
                    case 4:
                        imageResource = R.drawable.pink;
                        break;
                    default:
                        imageResource = R.drawable.yellow ;

                }
                Drawable res = mContext.getResources().getDrawable(imageResource);
                innerImage.setImageDrawable(res);

                item.setShowThisRow(true);


        }
        item.setProcessed(true);
        text.setText(item.getWord());
        text.setTag(item.getDescription());
        return v;
    }
}