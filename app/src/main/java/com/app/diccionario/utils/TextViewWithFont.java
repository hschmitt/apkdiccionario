package com.app.diccionario.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by hacksy on 21/05/14.
 */
public class TextViewWithFont extends TextView {
    public  Typeface typeface ;
    public TextViewWithFont(Context context, AttributeSet attrs) {
        super(context, attrs);
        typeface  = Typeface.createFromAsset(context.getAssets(), "calibri.ttf");
        this.setTypeface(typeface);
    }

    public TextViewWithFont(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        typeface  = Typeface.createFromAsset(context.getAssets(), "calibri.ttf");
        this.setTypeface(typeface);
    }

    public TextViewWithFont(Context context) {
        super(context);
        typeface  = Typeface.createFromAsset(context.getAssets(), "calibri.ttf");

        this.setTypeface(typeface);
    }

}
